<?php

/**
 * Admin settings form
 */
function taxonomy_client_admin_settings() {
  $form = array();

  $form['taxonomy_client_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Taxonomy server'),
    '#description' => t('Url of the taxonomy server'),
    '#default_value' => variable_get('taxonomy_client_server_url', ''),
  );

  $form['#validate'][] = 'taxonomy_client_admin_settings_form_validate';

  return system_settings_form($form);
}

/**
 * Form validation callback for admin settings forms
 */
function taxonomy_client_admin_settings_form_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['taxonomy_client_server_url'], TRUE)) {
    form_set_error('taxonomy_client_server_url', t('The taxonomy server url must be a valid url')); // FIXME: include valid url specs
  }

  // @FIXME: catch change in taxonomy server: clean out mapping tables to avoid conflicts in ids?
}

function taxonomy_client_sync_form() {
  $form = array();

  // do a check
  $url = variable_get('taxonomy_client_server_url', '');
  if (strlen($url) == 0) {
    drupal_set_message(t('The settings for the taxonomy server are not set. Synchronisation will fail. Go !settings_page to set them.', array('!settings_page' => l(t('here'), 'admin/config/taxonomy_client/settings'))), 'error');
  }

  $form['taxonomy_client_msg'] = array(
    '#markup' => t('Do you want to pull in the latest changes to the global taxonomy from the taxonomy server?')
  );

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['taxonomy_client_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Yes'),
  );

  $form['actions']['taxonomy_client_cancel'] = array(
    '#markup' => l(t('No'), 'admin/structure/taxonomy'),
  );

  return $form;
}

function taxonomy_client_sync_form_submit($form, &$form_state) {
   module_load_include('inc', 'taxonomy_client', 'taxonomy_client.sync');

  //do a test import/update
  taxonomy_client_synchronise();

  drupal_set_message(t('Taxonomies are in sync now'));
  drupal_goto('admin/structure/taxonomy');
}