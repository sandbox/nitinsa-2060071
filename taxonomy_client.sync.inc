<?php
// $Id$

/**
 * @file
 *
 * Synchronisation of taxonomy vocabularies code
 */

/**
 * Callback for synchronisation of taxonomy vocabularies
 *
 * @return Type    Description
 */
function taxonomy_client_synchronise() {
  module_load_include('inc', 'taxonomy_client', 'taxonomy_client.server');

  // Get all vocabularies on the taxonomy server
  $global_vocabularies = taxonomy_client_get_global_vocabularies();
  if (count($global_vocabularies) > 0) {
    foreach ($global_vocabularies as $vocabulary) {
    // Check if there is need to update
      if ($vocabulary->last_update > variable_get('taxonomy_client_' . $vocabulary->machine_name . '_last_update', 0)) {
        // Save vocabulary
        $new_last_update = $vocabulary->last_update;
        $global_vid = $vocabulary->vid;

        $local_vid = NULL;
        $local_vid = _taxonomy_client_import_taxonomy_vocabulary($vocabulary);

        if ($local_vid > 0) {
          // Get remote terms
          $tree = taxonomy_client_get_global_terms($global_vid);
          $success = FALSE;

          // Save terms
          $success = _taxonomy_client_import_taxonomy_terms($local_vid, $tree);

          // Save last update from server
          if ($success) {
            variable_set('taxonomy_client_' . $vocabulary->machine_name . '_last_update', $new_last_update);
          }
        }
        elseif ($local_vid == -1) {
          // Machine name already exists, we can't do the import
          watchdog('taxonomy', t('Global vocabulary @voc_name (machine name: @voc_machine_name ) could not be imported from the master because there is a local taxonomy with the same machine name. The vocabulary will not be imported.', array('@voc_name' => $vocabulary->name, '@voc_machine_name' => $vocabulary->machine_name)), array(), WATCHDOG_WARNING);
        }
        else {
          // @FIXME: unknown error
        }

      }
    }

    // @TODO: some reporting?

    watchdog('taxonomy', 'Global taxonomy vocabularies have been synchronised.', array(), WATCHDOG_INFO);
  }
  else {
    watchdog('taxonomy', 'There are no global vocabularies to import', array(), WATCHDOG_INFO);
  }
}


/**
 * Save global vocabulary locally
 *
 * @param object $global_vocabulary Vocabulary object
 *
 * @return integer    Vocabulary id of the locally saved vocabulary or -1
 */
function _taxonomy_client_import_taxonomy_vocabulary($global_vocabulary) {
  // If machine name exists, don't sync
  $query = db_select('taxonomy_vocabulary', 'v')
    ->condition('v.machine_name', $global_vocabulary->machine_name)
    ->fields('v', array('vid'));
  $name_exists = $query->countQuery()->execute()->fetchField();

  // Check if vocabulary is already imported
  $local_vid = db_select('taxonomy_client_global_vocabulary', 'gv')
    ->condition('gv.global_vid', $global_vocabulary->vid)
    ->fields('gv', array('local_vid'))
    ->execute()
    ->fetchField();

  if ($local_vid) {
    //it exists
    $global_vocabulary->vid = $local_vid;
    taxonomy_vocabulary_save($global_vocabulary);
  }
  else {
    //new voc
    if ($name_exists > 0) {
      return -1;
    }
    $global_vid = $global_vocabulary->vid;
    unset($global_vocabulary->vid);
    taxonomy_vocabulary_save($global_vocabulary);
    $local_vid = $global_vocabulary->vid;

    //save mapping in db
    $record = (object) array('global_vid' => $global_vid, 'local_vid' => $local_vid);
    drupal_write_record('taxonomy_client_global_vocabulary', $record);
  }

  return $local_vid;

}

/**
 * Save global taxonomy tree locally
 *
 * @param integer $local_vid         Local vocabulary id
 * @param array $global_tree       Array of global taxonomy terms
 *
 * @return integer    Status
 */
function _taxonomy_client_import_taxonomy_terms($local_vid, $global_tree) {

  // @TODO: move db to db functions
  $local_terms = taxonomy_client_db_get_tid_mapping($local_vid);
  $imported_terms = array();

  // Basic check for valid data
  if (is_array($global_tree)) {
    foreach ($global_tree as $term) {
      // @TODO: move this code to it's own function?

      $term->vid = $local_vid;

      // Fix the parent ids (mind the difference in property name)
      $term->parent = array();
      foreach ($term->parents as $parent) {
        if ((int)$parent > 0) {
          $term->parent[] = $local_terms[$parent];
        }
      }
      if (count($term->parent) == 0) {
        unset($term->parent);
      }

      // Do update or insert
      $global_tid = $term->tid;
      $term->vid = $local_vid;
      if (array_key_exists($global_tid, $local_terms)) {
        $term->tid = $local_terms[$term->tid];
        taxonomy_term_save($term);
        $local_tid = $term->tid;
      }
      else {
        unset($term->tid);
        taxonomy_term_save($term);
        $local_tid = $term->tid;

        // Save mapping in db
        $record = (object) array('global_tid' => $global_tid, 'local_tid' => $local_tid);
        drupal_write_record('taxonomy_client_global_term', $record);
        // Update mapping table, assuming that the order is correct
        $local_terms[$global_tid] = $local_tid;
      }
      // Save for cleanup
      $imported_terms[$global_tid] = $local_tid;
    }
  }

  // Delete local terms
  $delete_terms = array_diff_key($local_terms, $imported_terms);
  foreach ($delete_terms as $global_tid => $local_tid) {
    //these terms don't exist in the global taxonomy any more
    taxonomy_term_delete($local_tid);
    db_delete('taxonomy_client_global_term')->condition('global_tid', $global_tid)->execute();
  }

  // Assemble the status of all the operations
  return TRUE;
}

/**
 * Get global vocabularies from taxonomy master
 *
 * @return array    List of vocabulary objects from master server
 */
function taxonomy_client_get_global_vocabularies() {
  $server_vocabularies = taxonomy_client_server_get_data('VOCABULARY_LIST');

  $vocabularies = array();
  if (is_array($server_vocabularies)) {
    foreach ($server_vocabularies as $v) {
      // Need to do an extra request for detailed info
      $vocabularies[] = taxonomy_client_server_get_data('VOCABULARY_ITEM', array('vid' => $v->vid));
    }
  }

  return $vocabularies;
}

/**
 * Get global terms tree from server
 *
 * @param int $vid Vocabulary Id
 *
 * @return array    Taxonomy tree
 */
function taxonomy_client_get_global_terms($vid) {
  $tree = taxonomy_client_server_get_data('VOCABULARY_TREE', array('vid' => $vid));
  // @TODO : Check for valid data here
  return $tree;
}

/**
 * Get global vocabulary mapping from database
 * WTF: does not get used at the moment ... im sure i'll find some use for it in the future
 *
 * @return array    Vocabulary mapping
 */
function taxonomy_client_db_get_global_vocabularies() {
  $query = db_select('taxonomy_client_global_vocabulary', 'gv')->fields('gv', array('global_vid', 'local_vid'));
  $db_result = $query->execute();

  $vocabularies = array();
  foreach ($db_result as $voc) {
    $vocabularies[] = $voc;
  }
  return $vocabularies;
}

/**
 * Helper function to get mapping table for tids
 *
 * @return array    Array with global and local tids
 */
function taxonomy_client_db_get_tid_mapping($local_vid) {
  $local_term_query = db_select('taxonomy_client_global_term', 'gt')
    ->fields('gt', array('global_tid', 'local_tid'))
    ->condition('t.vid', $local_vid, '=');
  $local_term_query->join('taxonomy_term_data', 't', ' gt.local_tid = t.tid ');
  $local_term_result = $local_term_query->execute();

  $local_terms = array();
  foreach ($local_term_result as $local_result) {
    $local_terms[$local_result->global_tid] = $local_result->local_tid;
  }

  return $local_terms;
}