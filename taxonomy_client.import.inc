<?php

/**
 * Summary
 *
 * @return Type    Description
 */
function taxonomy_client_import() {
  //get global vocabularies
  //get vocabularies from server
  //check last modification date
  //getTree for each repo if necessary
  //retrieve each term and update

  $global_vocabularies = _taxonomy_client_get_server_vocabularies();

  foreach ($global_vocabularies as $vocabulary) {
    if ($vocabulary->last_update > variable_get('taxonomy_client_' . $vocabulary->vid . '_last_update', 0)) {
      //we need to update or insert
      $tree = _taxonomy_client_get_server_terms($vocabulary->vid);
      _taxonomy_client_import_taxonomy_terms($vocabulary, $tree);

      //save last update from server
      variable_set('taxonomy_client_' . $vocabulary->vid . '_last_update', $vocabulary->last_update);
    }
    else {
      //wee, we can skip this one
    }
  }
}

function _taxonomy_client_get_server_vocabularies() {
  $server_url = variable_get('taxonomy_client_server_url', '');
  $encoding = 'json';
  $url = $server_url . '/taxport_rest/taxonomy_vocabulary' . '.' . $encoding;

  $server_vocabularies = _taxonomy_client_do_taxonomy_server_request($url, $encoding);

  $vocs = array();
  foreach ($server_vocabularies as $v) {
    $vocs[] = _taxonomy_client_do_taxonomy_server_request($v->uri, $encoding);
  }
  return $vocs;
}


function _taxonomy_client_get_global_vocabularies() {
  $query = db_select('taxonomy_client_global_vocabulary', 'gv')->fields('gv', array('global_vid', 'local_vid'));
  $db_result = $query->execute();

  $vocabularies = array();
  foreach ($db_result as $voc) {
    $vocabularies[] = $voc;
  }
  return $vocabularies;
}


// helper
function _taxonomy_client_do_taxonomy_server_request($url, $encoding, $method = 'GET', $parameters = array()) {

  $options = array('method' => $method);
  $options['headers'] = array(
    'Accept' => 'application/json',
    'Content-Type' => 'application/x-www-form-urlencoded',
  );
  if (count($parameters)) {
    $options['data'] = http_build_query($parameters);
  }

  $response = drupal_http_request($url, $options);

  if ($response->code != '200') {
    throw new Exception(t('Error retrieving data from server for url !url', array('!url' => $url)), TAXONOMY_MASTER_CANNOT_RETRIEVE);
  }

  $data = $response->data;
  if ($encoding == 'json') {
    $content = json_decode($data);
  }

  return $content;
}

function _taxonomy_client_get_server_terms($vid) {
  $server_url = variable_get('taxonomy_client_server_url', '');
  $encoding = 'json';
  $url = $server_url . '/taxport_rest/taxonomy_vocabulary/getTree' . '.' . $encoding;

  $tree = _taxonomy_client_do_taxonomy_server_request($url, $encoding, 'POST', array('vid' => $vid));

  // @TODO: get extra info for tree items from server? -> later

  return $tree;
}

function _taxonomy_client_import_taxonomy_terms($global_vocabulary, $global_tree) {
  //check if global vid

  $query = db_select('taxonomy_client_global_vocabulary', 'gv')
    ->condition('global_vid', $global_vocabulary->vid)
    ->fields('gv', array('local_vid'))
    ->execute();
  $local_vid_exists = $query->rowCount;

  if ($local_vid) {
    //it exists
    $global_vocabulary->vid = $local_vid;
    taxonomy_vocabulary_save($global_vocabulary);
  }
  else {
    //new voc
    $global_vid = $global_vocabulary->vid;
    unset($global_vocabulary->vid);
    taxonomy_vocabulary_save($global_vocabulary);
    $local_vid = $global_vocabulary->vid;

    //save mapping in db
    $record = (object) array('global_vid' => $global_vid, 'local_vid' => $local_vid);
    drupal_write_record('taxonomy_client_global_vocabulary', $record);
  }

  //do tree: trivial
}