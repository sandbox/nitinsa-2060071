<?php
// $Id$

/**
 * @file
 *
 * Server request functions
 */

/**
 * Server function: Get data from the taxonomy server
 *
 * @param string $type   Identifier of the type of data to get
 * @param array $params Parameters to pass
 *
 * @return array    Server data, as array of objects
 */
function taxonomy_client_server_get_data($type, $params = array()) {

  // Sensible defaults
  $url = '';
  $request_type = 'GET';
  $request_params = $params;

  // Server url
  $server_url = variable_get('taxonomy_client_server_url', '');

  // Service definition on the server. @TODO: move to configuration?
  $server_service = 'taxport_rest';

  switch ($type) {
    case 'VOCABULARY_LIST':
      $action = 'taxonomy_vocabulary';
      break;

    case 'VOCABULARY_ITEM':
      $action = 'taxonomy_vocabulary' . '/' . $params['vid'];
      break;

    case 'VOCABULARY_TREE':
      $action = 'taxonomy_vocabulary/getTree';
      $request_type = 'POST';
      $request_params = array('vid' => $params['vid']);
      break;

    default:
      throw Exception('No type give for server request');
  }

  // Create the full url
  $url = $server_url . '/' . $server_service . '/' . $action;

  try {
    $data = _taxonomy_client_server_do_rest_request($url, $request_type, $request_params);
  }
  catch (Exception $e) {
    return $e->getMessage();
  }

  // @TODO?: check validity of the data

  return $data;
}

/**
 * Server function: do the actual request
 *
 * @param string $url        Url to request
 * @param string $method     Request method (GET of POST)
 * @param array $parameters Parameters for the request
 *
 * @return array    Data from the server
 */
function _taxonomy_client_server_do_rest_request($url, $method = 'GET', $parameters = array()) {

  // Set encoding for rest server: json or xml
  $encoding = 'json';
  $url .= '.' . $encoding;

  // Create options for HTTP request
  $options = array('method' => $method);
  $options['headers'] = array(
    'Accept' => 'application/json',
    'Content-Type' => 'application/x-www-form-urlencoded',
  );
  if (count($parameters)) {
    $options['data'] = http_build_query($parameters);
  }
  $response = drupal_http_request($url, $options);
  if ($response->code != '200') {
    throw new Exception(t('Error retrieving taxonomy data from server on !url', array('!url' => $url)), TAXONOMY_MASTER_CANNOT_RETRIEVE);
  }

  $data = $response->data;

  // Decode data based on chosen encoding
  if ($encoding == 'json') {
    $content = json_decode($data);
  }

  return $content;
}